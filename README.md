# Computação Concorrente

## Laboratório 3 - Valor maximo e minimo de um vetor, sequencial e concorrente

<br>

### Michel Monteiro Schorr 120017379

<br><br>

### Dentro da pasta lab, irá encontrar 3 pastas:  
**execs**, contendo os executaveis  
**libs**, contendo as bibliotecas, macros, funcoes, estruturas, etc  
**src**, contendo o condigo fonte principal

<br><br>

## Comandos

Os comandos para compilar e executar são os seguintes:
<br>
<br>

### funcao de achar valor maximo e minimo de um vetor

``` 
gcc ./lab/src/min_max_val_vector.c -o ./lab/execs/min_max_val_vector -lpthread
./lab/execs/min_max_val_vector dim threads
```
<br><br><br><br>

# Resultados

## dim 10^5 ______threads 1

### _________sequencial x concorrente

### teste 1: 0.000845 __ x __ 0.001451
### teste 2: 0.000991 __ x __ 0.001527
### teste 3: 0.000841 __ x __ 0.001510
### teste 4: 0.000845 __ x __ 0.001452
### teste 5: 0.000846 __ x __ 0.001451





<br><br>




## dim 10^5 ______threads 2

### _________sequencial x concorrente

### teste 1: 0.000849 __ x __ 0.001106
### teste 2: 0.000636 __ x __ 0.001106
### teste 3: 0.000846 __ x __ 0.001024
### teste 4: 0.000855 __ x __ 0.000970
### teste 5: 0.000859 __ x __ 0.001082





<br><br>

## dim 10^5 ______threads 4

### _________sequencial x concorrente

### teste 1: 0.001049 __ x __ 0.001045
### teste 2: 0.000845 __ x __ 0.000971
### teste 3: 0.000922 __ x __ 0.000978
### teste 4: 0.000839 __ x __ 0.000960
### teste 5: 0.000654 __ x __ 0.000832

<br><br>

## Resultados

### Tempo sequencial minimo: 0.000636
### Tempo concorrente minimo threads(1): 0.001451
### Tempo concorrente minimo threads(2): 0.000970
### Tempo concorrente minimo threads(4): 0.000832
### Aceleração threads(1): 0,44
### Aceleração threads(2): 0,66
### Aceleração threads(4): 0,76
<br><br><br><br>








## dim 10^7 ______threads 1

### _________sequencial x concorrente

### teste 1: 0.038893 __ x __ 0.039974
### teste 2: 0.040776 __ x __ 0.067730
### teste 3: 0.037908 __ x __ 0.066895
### teste 4: 0.043420 __ x __ 0.067679
### teste 5: 0.038443 __ x __ 0.067967



<br><br>




## dim 10^7 ______threads 2

### _________sequencial x concorrente

### teste 1: 0.042136 __ x __ 0.042961
### teste 2: 0.038444 __ x __ 0.042379
### teste 3: 0.039119 __ x __ 0.042489
### teste 4: 0.038007 __ x __ 0.042824
### teste 5: 0.038805 __ x __ 0.042585



<br><br>

## dim 10^7 ______threads 4

### _________sequencial x concorrente

### teste 1: 0.038322 __ x __ 0.022963
### teste 2: 0.038166 __ x __ 0.023129
### teste 3: 0.038039 __ x __ 0.029148
### teste 4: 0.037904 __ x __ 0.023205
### teste 5: 0.038218 __ x __ 0.023287



<br><br>

## Resultados

### Tempo sequencial minimo: 0.037904
### Tempo concorrente minimo threads(1): 0.039974
### Tempo concorrente minimo threads(2): 0.042379
### Tempo concorrente minimo threads(4): 0.022963
### Aceleração threads(1): 0,95
### Aceleração threads(2): 0,89
### Aceleração threads(4): 1,65
<br><br><br><br>








## dim 10^8 ______threads 1

### _________sequencial x concorrente

### teste 1: 0.386676 __ x __ 0.375413
### teste 2: 0.390046 __ x __ 0.401771
### teste 3: 0.385842 __ x __ 0.393356
### teste 4: 0.389915 __ x __ 0.399074
### teste 5: 0.395481 __ x __ 0.395738



<br><br>





## dim 10^8 ______threads 2

### _________sequencial x concorrente

### teste 1: 0.386778 __ x __ 0.222647
### teste 2: 0.385436 __ x __ 0.230793
### teste 3: 0.394565 __ x __ 0.216806
### teste 4: 0.400035 __ x __ 0.221602
### teste 5: 0.389749 __ x __ 0.223119



<br><br>

## dim 10^8 ______threads 4

### _________sequencial x concorrente

### teste 1: 0.386354 __ x __ 0.145798
### teste 2: 0.388528 __ x __ 0.152263
### teste 3: 0.389773 __ x __ 0.166229
### teste 4: 0.443821 __ x __ 0.146835
### teste 5: 0.644050 __ x __ 0.161961



<br><br>

## Resultados

### Tempo sequencial minimo: 0.385436
### Tempo concorrente minimo threads(1): 0.375413
### Tempo concorrente minimo threads(2): 0.216806
### Tempo concorrente minimo threads(4): 0.145798
### Aceleração threads(1): 1,03
### Aceleração threads(2): 1,78
### Aceleração threads(4): 2,64
<br><br><br><br>