

#ifndef _MACROS_H 
    
    #define create_threads(__newthread, __attr, __start_routine, __arg, nthreads) {                      \
    /*                                                                                                  \
        Wrapper para a função pthread_create()                                                             \
        recebe o(s) id(s) de sistema da(s) thread(s) alvo, os atributos para a thread                                   \
        a rotina/tarefa/task a ser executada pela thread, o(s) argumento(s) para a(s) thread(s) alvo        \
        e o numero de threads                                                                               \
    */                                                                         \
        for(long int i=0; i<nthreads; i++) {                                                             \
            if(pthread_create(__newthread+i, __attr, __start_routine, (void*) (__arg+i))){               \
                puts("ERRO--pthread_create"); exit(3);                                                   \
            }                                                                                            \
        }                                                                                                \
    }


    
    #define join_threads(__th, __thread_return, nthreads) {                     \
    /*                                                                                  \
        Wrapper para a função pthread_join()                                        \
        recebe o id de sistema da thread alvo, o endereço para guardar              \
        o valor retornado pelo pthread_exit() e o numero de threads requeridas          \
    */                                                                                          \
        for(int i=0; i<nthreads; i++) {                                         \
            pthread_join(*(__th+i), (void **)  __thread_return+i);              \
        }                                                                       \
    }
  
#endif