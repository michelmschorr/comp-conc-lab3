#include "../libs/include.h"


float* vector;
long long int dim;
int nthreads;








void * task(void * arg) {
    long long int startPos = (long long int ) arg;

    int jmp = nthreads;

    float* max; //max local da thread
    float* min; //min local da thread
    taskResults* min_max; //guarda o min e max no final para retornar



    max = (float *) malloc(sizeof(float));
    if(max==NULL) {exit(1);}
    min = (float *) malloc(sizeof(float));
    if(min==NULL) {exit(1);}
    min_max = (taskResults *) malloc(sizeof(taskResults));
    if(min_max==NULL) {exit(1);}
    



    *max = vector[startPos];
    *min = vector[startPos];

    for(long long int i=startPos; i < dim; i += jmp){
        
        if(vector[i]>*max) *max = vector[i];
        else if(vector[i]<*min) *min = vector[i];
        
    }

    min_max->min=*min;
    min_max->max=*max;

    free(max);
    free(min);


    pthread_exit((void*)min_max);
}






















int main(int argc, char* argv[]){
    pthread_t *tid; //identificadores das threads no sistema

    taskResults** taskResult;

    float seqMax;
    float seqMin;
    float concMax;
    float concMin;

    //vars para calculo de tempo
    double inicio, fim, delta, seqTime, concTime;

    //vars para gerar numero randomicos
    float ceiling = 500.0;
    float a = ((float)RAND_MAX)/(2*ceiling);



    //recebe dimensao e numero de threads
    if(argc < 3) {
       fprintf(stderr, "Digite: %s <dimensao do vetor> <numero threads>\n", argv[0]);
       return 1; 
    }

    dim = atoll(argv[1]);   
    nthreads = atoi(argv[2]);

    if(nthreads > dim) nthreads = dim;


    

    //inicializa vetor e outras variaveis
    tid = (pthread_t *) malloc(sizeof(pthread_t) * nthreads);
    if(tid==NULL) {exit(1);}

    vector = (float *)malloc(sizeof(float)*dim);
    if(vector==NULL) {exit(1);}

    taskResult = (taskResults **)malloc(sizeof(taskResults**)*nthreads);
    if(taskResult==NULL) {exit(1);}
    





    //preenche o vetor
    srand ( time(NULL) );
    for(long long int i=0; i< dim; i++){
        vector[i] = ((float)rand()/a)-ceiling;
    }






    //cria threads
    GET_TIME(inicio);

    create_threads(tid, NULL, task, NULL, nthreads);




    //join threads                                                            
    join_threads(tid, taskResult, nthreads);


    //Recupera o max e min dos retornos da threads
    concMin = taskResult[0]->min;
    concMax = taskResult[0]->max;

    for(int i=1; i<(nthreads); i++){
        if(taskResult[i]->max > concMax) concMax = taskResult[i]->max;
        if(taskResult[i]->min<concMin) concMin = taskResult[i]->min;
    }

    GET_TIME(fim);
    delta = fim - inicio;
    concTime = delta;






    //resolve sequencialmente
    GET_TIME(inicio);


    seqMax = vector[0];
    seqMin = vector[0];

    for(long long int i=1; i< dim; i++){
        if(vector[i]>seqMax) seqMax = vector[i];
        else if(vector[i]<seqMin) seqMin = vector[i];
    }


    GET_TIME(fim);
    delta = fim - inicio;
    seqTime = delta;




    
    //printa resultados sequencias
    if(seqMax == concMax && seqMin == concMin) printf("---- Resultados iguais ----\n");
    else{
        printf("---- Resultados diferentes ----\n");
        return 3;
    }
    printf("SeqMax:  %f\nSeqMin:  %f\n", seqMax, seqMin);
    printf("Tempo sequencial: %lf\n", seqTime);

    printf("\n");




    //printa resultados concorrentes
    printf("ConcMax: %f\nConcMin: %f\n", concMax, concMin);
    printf("Tempo concorrente:%lf\n", concTime);
    



    //printa a aceleração
    //printf("\n");   
    //printf("Aceleração: %lf\n", seqTime/concTime);
    



    //printa o vetor para testes
    /*
    for(long long int i=0; i< dim; i++){
        printf("%f\n", vector[i]);
    }
    */



    //free na memoria
    free(vector);
    free(tid);
    free(taskResult);





    return 0;
}